package entities;

/**
 * Represents a missing item, the item could be a product that already
 * exists on the inventory or new products that the business wants to 
 * start distributing.
 * 
 * @author luisburgos
 */
//TODO: This is a "Demand's list item"
public class Item {
    
    private String name;
    private String brand;
    private int id;
    
    public Item(){}

    /**
     * Creates a new simple object which represents a wished item.
     * @param name the missing item's name.
     * @param brand of the item.
     */
    public Item(String name, String brand) {
        this.name = name;
        this.brand = brand;
    }
    
    /**
     * Creates a new simple object which represents a wished item.
     * @param id of the item
     * @param name the missing item's name.
     * @param brand of the item.
     */
    public Item(int id, String name, String brand) {
        this.id = id;
        this.name = name;
        this.brand = brand;
    }

    /**
     * @return a String representing the missing item's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the missing item's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }
    
    public String getBrand(){
        return brand;
    }
    
    public void setBrand(String brand){
        this.brand = brand;
    }
    
    @Override
    public String toString(){
       return getName() + " - " + getBrand();
    }
}
