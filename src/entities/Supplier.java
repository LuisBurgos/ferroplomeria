package entities;

import java.util.Objects;

/**
 * Holds information about a particular products supplier.
 *
 * @author luisburgos
 */
public class Supplier {
    
    public Supplier(){}

    private int id;
    private String name;
    private String phone;
    private String email;
    private String address;

    /**
     * Creates a new products supplier
     *
     * @param name supplier's name.
     * @param phone supplier's phone number for contact.
     * @param email supplier's email address e.g. "supplier@gmail.com".
     * @param address physical address of the supplier.
     */
    public Supplier(
            String name,
            String phone,
            String email,
            String address
    ) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    /**
     * Creates a new products supplier
     *
     * @param id supplier's identifier.
     * @param name supplier's name.
     * @param phone supplier's phone number for contact.
     * @param email supplier's email address e.g. "supplier@gmail.com".
     * @param address physical address of the supplier.
     */
    public Supplier(
            int id,
            String name,
            String phone,
            String email,
            String address
    ) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    /**
     * @return an Integer representing the supplier's identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the supplier's identifier.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return a String representing the supplier's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name supplier's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return a String representing the supplier's phone number. 
 The number will be in 10 digit format e.g.
 "9999851420"
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the supplier's phone number. 
 The number should be in 10 digit format e.g. "9999851420"
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return a String representing the supplier's email address.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the supplier's email address e.g. "suppliers@gmail.com"
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return a String representing the physical address of the supplier. 
     * The address does not have a defined format.
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the physical address of the supplier.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.phone);
        hash = 59 * hash + Objects.hashCode(this.email);
        hash = 59 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Supplier other = (Supplier) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    
    
    

}
