
package business.coordinators;

import business.interfaces.business.Coordinator;
import business.process.OrderAdditionProcess;
import business.process.OrderRemoveProcess;
import data.daos.OrderDAO;
import entities.Order;
import entities.OrderItem;
import java.util.List;

public class OrderCoordinator extends Coordinator<Order> {
    
    private final OrderDAO dataAccessObject;


    public OrderCoordinator() {
        dataAccessObject = new OrderDAO();
    }

    @Override
    public boolean add(Order entity) {
        OrderAdditionProcess process = new OrderAdditionProcess(entity);
        return process.execute();
    }

    @Override
    public boolean remove(int id) {
        Order deleteOrder = get(id);
        dataAccessObject.close();
        OrderRemoveProcess process = new OrderRemoveProcess(get(id));
        return process.execute();
    }

    @Override
    public boolean edit(Order entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Order get(int id) {
        return dataAccessObject.getById(id);
    }

    @Override
    public Order[] getList() {
        return (Order[]) dataAccessObject.getList();
    }
    
    public String getSummary(Order order){
        List<OrderItem> allItems = order.getItems();
        String content = "";
        for(OrderItem current : allItems){
            content = content + current.getName() + " (" + current.getQuantity() + "), ";
        }
        return content;
    }

}
