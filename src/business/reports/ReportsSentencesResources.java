/*
 * To change this license DOCUMENT_TICKET_TABLE_HEADER, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package business.reports;

/**
 *
 * @author luisburgos
 */
public class ReportsSentencesResources {

    public static final String DOCUMENT_TITLE_HEADER = "Reporte de Ventas: ";
    public static final String DOCUMENT_BREAK_LINE = "\n";
    public static final String DOCUMENT_EXTENSION = ".pdf";
    public static final String DOCUMENT_NAME_DELIMETER = "//";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd_hh:mm:ss";
    public static final String NO_TICKETS_FROM_DATES = "No existen ventas para ese período";
    public static final String YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";

    public static final String[] DOCUMENT_TICKET_TABLE_HEADER = { "Producto", "Cantidad", "Precio unitario" };

}
