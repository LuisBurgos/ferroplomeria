package business.process;

import data.daos.OrderDAO;
import data.daos.OrderItemDAO;
import entities.Order;
import entities.OrderItem;

public class OrderAdditionProcess {
    
    private final Order newOrder;
    private final OrderItemDAO orderItemDAO;
    private final OrderDAO orderDAO;
    
    public OrderAdditionProcess(Order order) {
        this.newOrder = order;
        orderItemDAO = new OrderItemDAO();
        orderDAO = new OrderDAO();
    }
    
    public boolean execute(){
        boolean isProcessSucessfull;
        
        isProcessSucessfull = registerNewOrder();
        if(!isProcessSucessfull){
            return false;
        }
        
        int newOrderID = getLastOrderID();
        if(newOrderID == orderDAO.INVALID_ORDER_ID){
            return false;
        }
        
        newOrder.setId(newOrderID);
        
        orderDAO.close();
        
        isProcessSucessfull = regirterOrderItems();
        return isProcessSucessfull;
    }
    
    private boolean registerNewOrder(){
        return orderDAO.insert(newOrder);
    }
    
    private int getLastOrderID(){
        return orderDAO.getLastInsertedIdAtTable();
    }
    
    private boolean regirterOrderItems(){
        boolean isRegisterDone = true;
        for(OrderItem current : newOrder.getItems()){
            current.setOrderID(newOrder.getId());
            boolean isCurrentOK = orderItemDAO.insert(current);
            isRegisterDone = isRegisterDone && isCurrentOK;
        }
        return isRegisterDone;
    }
    
}