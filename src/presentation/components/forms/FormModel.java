
package presentation.components.forms;

import java.awt.event.ActionEvent;
import presentation.exceptions.OperationErrorException;

public abstract class FormModel<Entity> {

    protected FormDialog dialog;
    private boolean isSubmitSucessfull;

    /**
     * Creates a new entity
     *
     * @param dialog that model will operate
     */
    public FormModel(FormDialog dialog) {
        this.dialog = dialog;
        isSubmitSucessfull = false;
        bindButtonWithOperation(); //Operation event linking
    }

    public abstract Entity compose(); //How to compose

    public abstract void fill(Entity entity); //How to fill from entity

    public abstract void executeOperation() throws OperationErrorException; //What operation will be done

    public boolean isSubmitSucessfull() {
        return isSubmitSucessfull;
    }

    public FormDialog getDialog() {
        return dialog;
    }

    public void showDialog() {
        dialog.setVisible(true);
    }

    private void closeDialog() {
        dialog.clearForm();
        dialog.dispose();
    }

    private void hideDialog() {
        dialog.setVisible(false);
    }

    private void bindButtonWithOperation() {
        dialog.submitForm(( ActionEvent e ) -> {
            try {
                executeOperation();
                isSubmitSucessfull = true;
                System.out.println("Es correcto");
            }
            catch ( OperationErrorException ex ) {
                isSubmitSucessfull = false;
            }
            finally {
                hideDialog();
                closeDialog();
            }
        });
    }

    public void updateDynamicData() {
        //Implementation on subclasses it's optional, that's why this is empty
    }

}
