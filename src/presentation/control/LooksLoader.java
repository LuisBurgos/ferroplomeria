
package presentation.control;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import presentation.resources.ColorResources;

public class LooksLoader {
    private static final String FONT_ARIAL_UNICODE = "Arial Unicode MS";
    private static final int TABLE_FONT_DEFAULT_SIZE = 10;

    public static void load() {
        //Background colors        
        setBackgroundColor(ColorResources.BLUE_PALE);
        setTextFieldBackgroundColor(ColorResources.WHITE_SNOW);
        setTableBackgroundColor(ColorResources.WHITE_SNOW);
        setMenuBarBackgroundColor(ColorResources.BLUE_PALE);

        //Fonts
        Font tableFont = new Font(FONT_ARIAL_UNICODE, Font.PLAIN, TABLE_FONT_DEFAULT_SIZE);
        setTableFont(tableFont);

        updateGraphics();
    }

    private static void updateGraphics() {
        for ( WindowReference windowReference : WindowReference.values() ) {
//            SwingUtilities.updateComponentTreeUI(WindowManager.getInstance().getWindow(windowReference));
        }

        for ( DialogReference dialogReference : DialogReference.values() ) {
//            SwingUtilities.updateComponentTreeUI(WindowManager.getInstance().getForm(dialogReference).getDialog());
        }
    }

    private static void setTableFont(Font font) {
        UIManager.put("Table.font", font);
    }

    private static void setBackgroundColor(Color color) {
        UIManager.put("Panel.background", color);
    }

    private static void setTextFieldBackgroundColor(Color color) {
        UIManager.put("TextField.background", color);
    }

    private static void setMenuBarBackgroundColor(Color color) {
        UIManager.put("MenuBar.background", color);
    }

    private static void setTableBackgroundColor(Color color) {
        UIManager.put("Table.background", color);
    }
}
