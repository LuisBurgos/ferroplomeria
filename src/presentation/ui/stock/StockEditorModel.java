package presentation.ui.stock;

import business.coordinators.ProductCoordinator;
import business.interfaces.business.Coordinator;
import entities.Category;
import entities.Item;
import entities.Product;
import javax.swing.JOptionPane;
import presentation.components.forms.FormDialog;
import presentation.components.forms.FormModel;
import presentation.control.status.StatusDialog;
import presentation.exceptions.OperationErrorException;
import presentation.resources.StringResources;

public class StockEditorModel extends FormModel<Product> {

    protected StockFormDialog castedForm;
    protected Coordinator<Product> productCoordinator;
    
    protected int productID;
    protected double productCost;
    protected double productPrice;
    protected String description;
    protected Item productItem;
    
    public StockEditorModel(FormDialog dialog) {
        super(dialog);
        castedForm = (StockFormDialog) dialog;
        productCoordinator = new ProductCoordinator();
        castedForm.setEditMode();
    }

    @Override
    public Product compose() {
        return new Product(
                productID,
                productItem,
                description,
                productCost,
                productPrice,
                castedForm.getStock(),
                ((Category)castedForm.getOption())
        );
    }

    @Override
    public void fill(Product entity) {
        productItem = entity.getItem();
        productID = entity.getId();
        castedForm.nameField.setText(entity.getName());
        description = entity.getDescription();
        castedForm.brandField.setText(entity.getBrand());
        productCost = entity.getCost();
        productPrice = entity.getPrice();
        castedForm.stockField.setText(String.valueOf(entity.getStock()));
        castedForm.fillOptions(new Object[]{entity.getCategory()});
        castedForm.setOption(entity.getCategory());
    }

    @Override
    public void executeOperation() throws OperationErrorException{
        Product toEdit = compose();
        if(productCoordinator.edit(toEdit)){
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_STOCK_SUCCESS_MESSAGE, toEdit.getName(),String.valueOf(toEdit.getStock())),
                    null, 
                    JOptionPane.INFORMATION_MESSAGE
            ).setVisible(true);
        } else {
            new StatusDialog(
                    castedForm,
                    StringResources.getResource(StringResources.EDIT_STOCK_FAIL_MESSAGE, toEdit.getName()),
                    null, 
                    JOptionPane.ERROR_MESSAGE
            ).setVisible(true);
            throw new OperationErrorException();
        }
    }
    
    
    
}
