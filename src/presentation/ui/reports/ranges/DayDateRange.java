/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.ui.reports.ranges;

import business.reports.date.DateCalculator;
import java.util.Date;

/**
 *
 * @author luisburgos
 */
public class DayDateRange extends DateRange {

    public DayDateRange(){        
        initializeDayReport();
    }

    private void initializeDayReport() {
        Date today = DateCalculator.getTodayDateWithoutTime();
        setInitialDate(today);        
        //Date dateTo = DateCalculator.incrementMillsOfDate(today);
        setFinalDate(today);     
    }
    
}
