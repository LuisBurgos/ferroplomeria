package presentation.ui.reports.ranges;

import java.util.Date;

public class DateRange {

    private Date initialDate;
    private Date finalDate;

    public DateRange() {
    }

    public DateRange(Date initialDate, Date finalDate) {
        this.initialDate = initialDate;
        this.finalDate = finalDate;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date newInitialDate) {
        this.initialDate = newInitialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date newFinalDate) {
        this.finalDate = newFinalDate;
    }

}
