
package presentation.ui.sales;

import business.management.Salesman;
import javax.swing.JButton;
import javax.swing.JFrame;
import presentation.components.tables.Table;
import presentation.components.tables.TableModel;

public class SalesViewer extends JFrame  {

    private final SalesViewerHandler viewerHandler;

    public SalesViewer() {
        initComponents();
        viewerHandler = new SalesViewerHandler(this);
    }

    public JButton getRemoveItemButton(){
        return removeItemButton;
    }

    public JButton getAddItemButton(){
        return addItemButton;
    }

    public JButton getFinishSaleButton(){
        return finishSaleButton;
    }

    public void setTotalAmountTextLabel(String totalAmount){
        totalAmountLabel.setText(totalAmount);
    }

    public Table getProductsTable(){
        return productsTable;
    }

    //<editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        mainPanel = new javax.swing.JPanel();
        mainScrollPane = new javax.swing.JScrollPane();
        String[] tableHeader = new String[]{ "Nombre", "Cantidad", "Precio" };
        boolean[] tablevisibility = { true, true, true };
        productsTable = new presentation.components.tables.Table();
        saleInformationPanel = new javax.swing.JPanel();
        totalLabel = new javax.swing.JLabel();
        totalAmountLabel = new javax.swing.JLabel();
        addItemButton = new javax.swing.JButton();
        removeItemButton = new javax.swing.JButton();
        finishSaleButton = new javax.swing.JButton();

        setTitle("Nueva Venta");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new java.awt.GridBagLayout());

        mainScrollPane.setName("mainScrollPane"); // NOI18N

        productsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        productsTable.setName("productsTable"); // NOI18N
        mainScrollPane.setViewportView(productsTable);
        productsTable.loadDefaultSettings();
        productsTable.setHeader(tableHeader, tablevisibility);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        mainPanel.add(mainScrollPane, gridBagConstraints);

        saleInformationPanel.setName("saleInformationPanel"); // NOI18N
        saleInformationPanel.setLayout(new java.awt.GridBagLayout());

        totalLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        totalLabel.setText("Total:");
        totalLabel.setName("totalLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        saleInformationPanel.add(totalLabel, gridBagConstraints);

        totalAmountLabel.setText("$0.00");
        totalAmountLabel.setName("totalAmountLabel"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(10, 2, 0, 10);
        saleInformationPanel.add(totalAmountLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        mainPanel.add(saleInformationPanel, gridBagConstraints);

        addItemButton.setText("+");
        addItemButton.setName("addItemButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        mainPanel.add(addItemButton, gridBagConstraints);

        removeItemButton.setText("-");
        removeItemButton.setName("removeItemButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(removeItemButton, gridBagConstraints);

        finishSaleButton.setText("Finalizar");
        finishSaleButton.setName("finishSaleButton"); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        mainPanel.add(finishSaleButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(mainPanel, gridBagConstraints);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>

    // Variables declaration - do not modify
    private javax.swing.JButton addItemButton;
    private javax.swing.JButton finishSaleButton;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JScrollPane mainScrollPane;
    private presentation.components.tables.Table productsTable;
    private javax.swing.JButton removeItemButton;
    private javax.swing.JPanel saleInformationPanel;
    private javax.swing.JLabel totalAmountLabel;
    private javax.swing.JLabel totalLabel;
    // End of variables declaration
    //</editor-fold>

    public void clear() {
        while (getProductsTable().getRowCount() > 0) {
            ((TableModel) getProductsTable().getModel()).removeRow(0);
        }
        setTotalAmountTextLabel("0.0");
    }

}
