/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation.ui.sales;

import business.management.Salesman;
import entities.Product;
import entities.TicketSaleItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;
import presentation.components.tables.TableModel;
import presentation.control.DialogReference;
import presentation.control.WindowManager;
import presentation.control.status.StatusDialog;

/**
 *
 * @author luisburgos
 */
public class SalesViewerHandler implements ActionListener {

    private final SalesViewer salesViewer;
    private Salesman salesman;

    public SalesViewerHandler(SalesViewer salesViewer) {
        this.salesViewer = salesViewer;
        this.salesman = new Salesman();
        salesViewer.getAddItemButton().addActionListener(this);
        salesViewer.getRemoveItemButton().addActionListener(this);
        salesViewer.getFinishSaleButton().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == salesViewer.getAddItemButton()) {
            handleAddItem();
        }

        if (e.getSource() == salesViewer.getRemoveItemButton()) {
            handleRemoveItem();
        }

        if (e.getSource() == salesViewer.getFinishSaleButton()) {
            handleFinishSale();
        }
    }

    private void handleAddItem() {
        TicketSaleItem newItem = getItemFromSaleDialog();
        salesman.addItemToTicketSale(newItem);
        salesman.updateTotalPayment();
        updateTotalAmountLabel();
    }

    private void handleRemoveItem() {
        TicketSaleItem selectedItem = getSelectedItemOnTable();
        salesman.removeItemToTicketSale(selectedItem);
        salesman.updateTotalPayment();
        updateTotalAmountLabel();
    }

    private void handleFinishSale() {
        salesman.finishCurrentSale();
        salesman = new Salesman();
        salesViewer.clear();
        showSuccessDialog();
    }

    private TicketSaleItem getItemFromSaleDialog() {
        SaleFormDialog saleAdder;
        saleAdder = requestAdderToWindowManager();
        Product selectedProduct;
        selectedProduct = saleAdder.getSelectedProduct();

        TicketSaleItem ticketSaleItem = new TicketSaleItem();
        if (selectedProduct != null) {
            ticketSaleItem.setProduct(selectedProduct);
            ticketSaleItem.setQuantity(saleAdder.getQuantity());
            ticketSaleItem.setUnitPrice(selectedProduct.getPrice());
            updateTableContent(ticketSaleItem);
        }

        return ticketSaleItem;
    }

    private TicketSaleItem getSelectedItemOnTable() {
        TicketSaleItem selectedItem = new TicketSaleItem();
        int selectedRow = salesViewer.getProductsTable().getSelectedRow();
        if (selectedRow >= 0) {
            String currentName = getNameFromSelectedRow(selectedRow);
            Product currentProduct = new Product();
            currentProduct.setName(currentName);
            selectedItem.setProduct(currentProduct);
            salesman.removeItemToTicketSale(selectedItem);
            ((TableModel) salesViewer.getProductsTable().getModel()).removeRow(selectedRow);
        }

        return selectedItem;
    }

    private SaleFormDialog requestAdderToWindowManager() {
        SaleFormDialog saleAdder;
        saleAdder = (SaleFormDialog) WindowManager.getInstance().getForm(DialogReference.SALE_ADDER).getDialog();
        saleAdder.setModal(true);
        saleAdder.setVisible(true);
        return saleAdder;
    }

    private String[] createStrinArrayFromTicket(TicketSaleItem ticketSaleItem) {
        return new String[]{
            ticketSaleItem.getProduct().getName(),
            String.valueOf(ticketSaleItem.getQuantity()),
            String.valueOf(ticketSaleItem.getProduct().getPrice())
        };
    }

    private void updateTableContent(TicketSaleItem ticketSaleItem) {
        String[] itemForTable = createStrinArrayFromTicket(ticketSaleItem);
        ((DefaultTableModel) salesViewer.getProductsTable().getModel()).addRow(itemForTable);
    }

    private void updateTotalAmountLabel() {
        salesViewer.setTotalAmountTextLabel(String.valueOf(salesman.getCurrentPaymentAmount()));
    }

    private String getNameFromSelectedRow(int selectedRow) {
        return String.valueOf(
                salesViewer.getProductsTable().getModel().getValueAt(
                        selectedRow, 0
                )
        );
    }

    private void showSuccessDialog() {
        new StatusDialog(salesViewer, "Se registró la venta con éxito").setVisible(true);
    }

}
