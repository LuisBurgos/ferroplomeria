package data;

import presentation.resources.StringResources;

public class ProductNotFoundException extends Exception {
    
    public ProductNotFoundException(){
        this(StringResources.PRODUCT_NOT_FOUND_EXCEPTION);
    }
    
    public ProductNotFoundException(String message) {
        super(message);
    }

    public ProductNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
}
