
package data.daos;

import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.CategoryQuerySentenceGenerator;
import entities.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity Category.
 *
 * @author Kirebyte
 */
public class CategoryDAO implements DAO<Category> {

    public static final String TABLE_CATEGORY = "CATEGORIES";
    private final CategoryQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;

    public CategoryDAO() {
        sentenceGenerator = new CategoryQuerySentenceGenerator(TABLE_CATEGORY);
        queryExecutor = new DatabaseQueryExecutor();
    }

    @Override
    public boolean insert(Category newCategory) {
        String insertCategorySentence;
        insertCategorySentence = sentenceGenerator.generateInsertCategorySentence(newCategory);
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertCategorySentence);
        }
        catch ( SQLException ex ) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }

    @Override
    public boolean delete(int existingProductID) {
        String deleteCategorySentence;
        deleteCategorySentence = sentenceGenerator.generateDeleteCategoryByIdSentence(existingProductID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteCategorySentence);
        }
        catch ( SQLException ex ) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;
    }

    @Override
    public boolean update(Category updatedCategory) {
        String updateCategorySentence;
        updateCategorySentence = sentenceGenerator.generateUpdateCategorySentence(updatedCategory);
        boolean updateDone = false;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateCategorySentence);
        }
        catch ( SQLException ex ) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }

    @Override
    public Category getById(int categoryID) {
        String findCategoryByIdSentence;
        findCategoryByIdSentence = sentenceGenerator.generateFindCategoryByIdSentence(categoryID);
        Category category = null;
        try {
            ResultSet resultSet = queryExecutor.executeFindByIdQuery(findCategoryByIdSentence);
            while ( resultSet.next() ) {
                category = EntityFromResultSetCreator.createCategoryFromResultSet(resultSet);
            }
        }
        catch ( SQLException ex ) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return category;
    }

    @Override
    public Category[] getList() {
        String findAllCategories;
        findAllCategories = sentenceGenerator.generateSelectAllFromTableSentence();
        ArrayList<Category> categories = new ArrayList<>();
        Category currentCategory;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllCategories);
            while ( resultSet.next() ) {
                currentCategory = EntityFromResultSetCreator.createCategoryFromResultSet(resultSet);
                categories.add(currentCategory);
            }
        }
        catch ( SQLException ex ) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categories.toArray(new Category[categories.size()]);
    }

}
