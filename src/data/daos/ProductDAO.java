package data.daos;

import business.interfaces.data.DAO;
import data.ProductNotFoundException;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import data.database.generators.ProductQuerySentenceGenerator;
import entities.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity Product.
 *
 * @author Erick López
 */
public class ProductDAO implements DAO<Product> {

    public static String TABLE_PRODUCTS = "PRODUCTS";
    private final ProductQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;

    public ProductDAO() {
        sentenceGenerator = new ProductQuerySentenceGenerator(TABLE_PRODUCTS);
        queryExecutor = new DatabaseQueryExecutor();
    }
    
    @Override
    public boolean insert(Product newProduct) {
        String insertProductSentence;
        insertProductSentence = sentenceGenerator.generateInsertProductSentence(newProduct);               
        boolean insertDone = false;
        try {
            insertDone = queryExecutor.executeInsertQuery(insertProductSentence);
        } catch (SQLException ex) {
            //TODO: Considerar arrojar una opción de alto nivel.           
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertDone;
    }

    @Override
    public boolean delete(int existingProductID) {
        String deleteProductSentence;
        deleteProductSentence = sentenceGenerator.generateDeleteProductByIdSentence(existingProductID);
        boolean deleteDone = false;
        try {
            deleteDone = queryExecutor.executeDeleteOperation(deleteProductSentence);
        } catch (SQLException ex) {
            //TODO: Considerar arrojar una opción de alto nivel.
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deleteDone;
    }

    @Override
    public boolean update(Product updatedProduct) {
        String updateProductSentence;
        updateProductSentence = sentenceGenerator.generateUpdateProductSentence(updatedProduct);        
        boolean updateDone = false;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateProductSentence);
        } catch (SQLException ex) {
            //TODO: Considerar arrojar una opción de alto nivel.
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }

    @Override
    public Product getById(int productID) {
        String findProductByIdSentence;
        findProductByIdSentence = sentenceGenerator.generateFindProductByIdSentence(productID);
        Product product = null;
        try {
            ResultSet resultSet = queryExecutor.executeFindByIdQuery(findProductByIdSentence);                   
            while (resultSet.next()) {
                product = EntityFromResultSetCreator.createProductFromResultSet(resultSet);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    @Override
    public Product[] getList() {
        String findAllProductsSentence;
        findAllProductsSentence = sentenceGenerator.generateFindAllProductsSentence();        
        ArrayList<Product> products = new ArrayList<>();
        Product currentProduct;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllProductsSentence);
            while (resultSet.next()) {
                currentProduct = EntityFromResultSetCreator.createProductFromResultSet(resultSet);
                products.add(currentProduct);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return products.toArray(new Product[products.size()]);
    }
    
    public Product[] getProductListByCategoryId(int categoryID) {
        String findAllProductsFromCategory;
        findAllProductsFromCategory = sentenceGenerator
                .generateFindAllProductsFromCategory(categoryID);
        
        return getProductsList(findAllProductsFromCategory);
    }

    public boolean updateProductStock(int productID, int newStockValue) {            
        String updateProductStockSentence;
        updateProductStockSentence = sentenceGenerator
                .generateUpdateStockSentence(newStockValue, productID);
        boolean updateDone = false;
        try {
            updateDone = queryExecutor.executeUpdateQuery(updateProductStockSentence);
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return updateDone;
    }
    
    public Product getProductByItemId(int itemId) throws ProductNotFoundException{
        String sentence;
        sentence = sentenceGenerator.generateFindProductByItemID(itemId);
        try{
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(sentence);
            if(resultSet.next()){
                return EntityFromResultSetCreator.createProductFromResultSet(resultSet);
            }
            else {
                throw new ProductNotFoundException();
            }
        } catch(SQLException ex){
            throw new ProductNotFoundException();
        }
    }

    private Product[] getProductsList(String query) {        
        ArrayList<Product> products = new ArrayList<>();
        Product currentProduct;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(query);
            while (resultSet.next()) {
                currentProduct = EntityFromResultSetCreator.createProductFromResultSet(resultSet);
                products.add(currentProduct);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products.toArray(new Product[products.size()]);
    }
    
}
