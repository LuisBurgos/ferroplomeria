package data.daos;

import data.database.generators.OrderItemQuerySentenceGenerator;
import business.interfaces.data.DAO;
import data.database.DatabaseQueryExecutor;
import data.database.EntityFromResultSetCreator;
import entities.OrderItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows database interaction with persistent entity OrderItem.
 *
 * @author Erick López
 */
public class OrderItemDAO implements DAO<OrderItem> {

    public static final String TABLE_ORDER_ITEMS = "ORDER_ITEMS";
    private final OrderItemQuerySentenceGenerator sentenceGenerator;
    private final DatabaseQueryExecutor queryExecutor;
    
    public OrderItemDAO(){
        sentenceGenerator = new OrderItemQuerySentenceGenerator(TABLE_ORDER_ITEMS);
        queryExecutor = new DatabaseQueryExecutor();
    }
    
    @Override
    public boolean insert(OrderItem newEntity) {
        boolean insertDone = false;
        String query;
        query = sentenceGenerator.generateInsertOrderItemSentence(newEntity);
        try{
            queryExecutor.executeInsertQuery(query);
            insertDone = true;
        } catch(SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return insertDone;
        }
    }

    @Override
    public boolean delete(int existingEntityID) {
        boolean deleteDone = false;
        String query;
        query = sentenceGenerator.generateDeleteOrderItemSentence(existingEntityID);
        try{
            queryExecutor.executeDeleteOperation(query);
            deleteDone = true;
        } catch(SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return deleteDone;
        }
    }

    @Override
    public boolean update(OrderItem updatedEntity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OrderItem getById(int entityID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OrderItem[] getList() {
        String findAllSentence;
        findAllSentence = sentenceGenerator.generateGetOrderItemListSentence();
        List<OrderItem> orders = new ArrayList<>();
        OrderItem current;
        try {
            ResultSet resultSet = queryExecutor.executeFindFromTableOperation(findAllSentence);
            while (resultSet.next()) {
                current = EntityFromResultSetCreator.createOrderItemFromResultSet(resultSet);
                orders.add(current);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SupplierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return orders.toArray(new OrderItem[orders.size()]);
    }
    
    public OrderItem[] getSpecificOrderList(int orderID){
        OrderItem[] allItems = getList();
        List<OrderItem> orderItemsList = new ArrayList<>(Arrays.asList(allItems));
        
        Iterator<OrderItem> iterator = orderItemsList.iterator();
        while(iterator.hasNext()) {
	    OrderItem current = iterator.next();
            if(current.getOrderID() != orderID){
                iterator.remove();
            }
        }
        return orderItemsList.toArray(new OrderItem[orderItemsList.size()]);
    }
    
    public void close(){
        queryExecutor.close();
    }

}
