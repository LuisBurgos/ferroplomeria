/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data.database.generators;

import static data.database.QueryStringResources.*;
import entities.Supplier;

/**
 *
 * @author luisburgos
 */
public class SupplierQuerySentenceGenerator {
    
    private final String tableName;

    public SupplierQuerySentenceGenerator(String tableName) {
        this.tableName = tableName;
    }   

    public String generateInsertSupplierSentence(Supplier newSupplier) {
        String generatedSentence;
        generatedSentence = INSERT_INTO + SPACE
                + tableName + SPACE
                + "(name, telephone, email, address)" + SPACE 
                + VALUES + SPACE + OPEN_PARENTHESIS                 
                + SINGLE_QUOTE + newSupplier.getName() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newSupplier.getPhone() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newSupplier.getEmail() + SINGLE_QUOTE + COMMA
                + SINGLE_QUOTE + newSupplier.getAddress() + SINGLE_QUOTE + CLOSE_PARENTHESIS
                + SEMICOLON;
        System.out.println(generatedSentence);
        return generatedSentence;
    }

    public String generateDeleteSupplierByIdSentence(int existingSupplierID) {
        String generatedSentence;
        generatedSentence = DELETE + SPACE
                + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS 
                + existingSupplierID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateUpdateSupplierSentence(Supplier updatedSupplier) {
        String condition = ID + EQUALS + updatedSupplier.getId();        
        String generatedSentence;
        generatedSentence = UPDATE + SPACE 
                + tableName + SPACE + SET + SPACE                                                 
                + "name=" + SINGLE_QUOTE + updatedSupplier.getName() + SINGLE_QUOTE + COMMA
                + "telephone=" + SINGLE_QUOTE + updatedSupplier.getPhone() + SINGLE_QUOTE + COMMA
                + "email=" + SINGLE_QUOTE + updatedSupplier.getEmail() + SINGLE_QUOTE + COMMA
                + "address=" + SINGLE_QUOTE + updatedSupplier.getAddress() + SINGLE_QUOTE
                + SPACE 
                + WHERE + SPACE 
                + condition 
                + SEMICOLON;             
        return generatedSentence;
    }

    public String generateFindSupplierByIdSentence(int supplierID) {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName + SPACE
                + WHERE + SPACE
                + ID + EQUALS
                + supplierID
                + SEMICOLON;
        return generatedSentence;
    }

    public String generateFindAllSuppliersSentence() {
        String generatedSentence;
        generatedSentence = SELECT
                + SPACE + ASTERISK
                + SPACE + FROM + SPACE
                + tableName
                + SEMICOLON;
        return generatedSentence;
    }
    
}
